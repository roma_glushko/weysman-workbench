#!/usr/bin/env bash

N98_FILE="n98";

if [ ! $(isInstalled n98) ];
then
    wget http://files.magerun.net/n98-magerun-latest.phar -O "${TEMP_DIR}/${N98_FILE}";
    sudo cp "${TEMP_DIR}/${N98_FILE}" ${LOCAL_BIN};
    sudo chmod +x "${LOCAL_BIN}/${N98_FILE}";
    rm "${LOCAL_BIN}/${N98_FILE}";
    printSuccess "N98-Magerun has been installed";
else
    printWarning "N98-Magerun has been already installed";
fi

n98 self-update
n98 --version