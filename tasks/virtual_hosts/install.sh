#!/usr/bin/env bash

APACHE_CONFIG="/etc/apache2/apache2.conf";
HOSTS_FILE="/etc/hosts";

VH_SAMPLE="test.dev.conf";
VH_DIRECTORY="/etc/apache2/sites-available";

VH_TEST="test.dev";
VH_TEST_PATH="${PROJECTS_DIR}/${VH_TEST}";

APACHE_USER="www-data";
APACHE_GROUP="www-data";

# configure virtual hosts for new directory

if [ ! $(cat ${APACHE_CONFIG} | grep "${PROJECTS_DIR}") ];
then
    echo "
<Directory ${PROJECTS_DIR}>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>
    " >> "${APACHE_CONFIG}";
fi

# configure test host

if [ -f "${VH_DIRECTORY}/${VH_SAMPLE}" ];
then
    printWarning "The sample of virtual has been already created";
else
    echo    "
<VirtualHost *:80>
    ServerName www.${VH_TEST}
    ServerAlias ${VH_TEST}
    ServerAdmin admin@test.dev
    DocumentRoot ${VH_TEST_PATH}
    <Directory ${VH_TEST_PATH}>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        Require all granted
        Allow from all
    </Directory>
    ErrorLog \${APACHE_LOG_DIR}/${VH_SAMPLE}-error.log
    CustomLog \${APACHE_LOG_DIR}/${VH_SAMPLE}-access.log combined
</VirtualHost>
    " > "${VH_DIRECTORY}/${VH_SAMPLE}";

    printSuccess "The sample of virtual has been created";
fi

if [ ! $(cat "${HOSTS_FILE}" | grep "test.dev") ];
then
    echo "127.0.0.1 test.dev" >> ${HOSTS_FILE};
fi

if [ ! -d "${VH_TEST_PATH}" ];
then
    mkdir "${VH_TEST_PATH}";
fi

if [ ! -f "${VH_TEST_PATH}/index.php" ];
then
    echo "<?php phpinfo();" >> "${VH_TEST_PATH}/index.php";
fi

sudo a2ensite ${VH_SAMPLE};
sudo service apache2 restart;

# configure permissions

sudo usermod -a -G ${CURRENT_USER} ${APACHE_GROUP};
sudo usermod -a -G ${APACHE_USER} ${CURRENT_USER};
sudo chown -R ${CURRENT_USER}:${CURRENT_USER} ${PROJECTS_DIR};
sudo chmod -R 777 ${PROJECTS_DIR};