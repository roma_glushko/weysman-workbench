#!/usr/bin/env bash

if [ ! $(isInstalled java) ];
then
    sudo apt-get install default-jre
    printSuccess "JRE has been installed";
else
    printSuccess "JRE has been already installed";
fi

java -version