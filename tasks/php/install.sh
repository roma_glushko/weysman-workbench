#!/usr/bin/env bash

export XDEBUG_CONFIG='/etc/php5/mods-available/xdebug.ini'

if [ $(isInstalled php5) ]
then
    printSuccess "Php5 has been already installed"
else
    sudo apt-get install php5
fi

php -v

# installing the extensions for the php

sudo apt-get install php5-common php5-curl php5-mysql php5-intl php5-gd php5-xsl php5-gmp php5-xdebug php5-mcrypt

sudo php5enmod mcrypt

# configure the xdebug

if [ $(cat ${XDEBUG_CONFIG} | grep "PHPSTORM") ]
then
    printSuccess "XDebug has been already configured"
else
    sudo echo "xdebug.remote_autostart=on" >> ${XDEBUG_CONFIG};
    sudo echo "xdebug.remote_enable=on" >> ${XDEBUG_CONFIG};
    sudo echo "xdebug.remote_handler=dbgp" >> ${XDEBUG_CONFIG};
    sudo echo "xdebug.remote_host=localhost" >> ${XDEBUG_CONFIG};
    sudo echo "xdebug.remote_mode=req" >> ${XDEBUG_CONFIG};
    sudo echo "xdebug.idekey=PHPSTORM" >> ${XDEBUG_CONFIG};
    sudo echo "xdebug.max_nesting_level=200" >> ${XDEBUG_CONFIG};
    printSuccess "XDebug has been configured"
fi

# set always_populate_raw_post_data to -1 here