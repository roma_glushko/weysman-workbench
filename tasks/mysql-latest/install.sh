#!/usr/bin/env bash

PACKAGE="mysql-apt-config_0.6.0-1_all.deb";
PACKAGE_URL="http://dev.mysql.com/get/${PACKAGE}";

if [ $(isInstalled mysql) ];
then
    printWarning "MySql has been already installed";
else
    # configure repositories to get new versions of mysql
    wget -O "${TEMP_DIR}/${PACKAGE}" "${PACKAGE_URL}";
    sudo dpkg -i "${TEMP_DIR}/${PACKAGE}";
    rm "${TEMP_DIR}/${PACKAGE}";
    sudo apt-get update
    # install MySql
    sudo apt-get install mysql-server
    printSuccess "MySql has been installed";
fi

mysql --version