#!/usr/bin/env bash

if [ ! $(isInstalled curl) ];
then
    sudo apt-get install curl libcurl3;
    printSuccess "CUrl has been installed!"
else
    printWarning "CUrl has been already installed!"
fi

curl --version