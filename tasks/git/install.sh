#!/usr/bin/env bash

SHORTCUT_FILE=".gitconfig"
SOURCE_SC_FILE="${CTD}res/${SHORTCUT_FILE}"

if [ $(isInstalled git) ]
then
    printSuccess "Git has been already installed"
else
    sudo apt-get install git
fi

git --version

if [ -f ${SC_FILE_DEST}${SHORTCUT_FILE} ]
then
    printSuccess "The git configuration has been already imported"
else
    \cp ${SOURCE_SC_FILE} ~/
    printSuccess "The git configuration has been imported"
fi

if [ $(isInstalled gitg) ]
then
    printSuccess "GitG has been already installed"
else
    sudo apt-get install gitg
fi

gitg -V