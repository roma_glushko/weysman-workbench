#!/usr/bin/env bash
INSTALLER="${TEMP_DIR}/composer-setup.php";
COMPOSER_FILE="composer";

if [ ! $(isInstalled composer) ]
then
    php -r "readfile('https://getcomposer.org/installer');" > "${INSTALLER}"
    php -r "if (hash('SHA384', file_get_contents('${INSTALLER}')) === 'fd26ce67e3b237fffd5e5544b45b0d92c41a4afe3e3f778e942e43ce6be197b9cdc7c251dcde6e2a52297ea269370680') { echo 'Installer verified'.PHP_EOL; } else { echo 'Installer corrupt'.PHP_EOL; unlink('${INSTALLER}'); }"
    sudo chmod +x "${INSTALLER}"
    sudo php ${INSTALLER} --install-dir=${LOCAL_BIN} --filename=${COMPOSER_FILE}
    php -r "unlink('${INSTALLER}');"

    sudo chmod +x "${LOCAL_BIN}/${COMPOSER_FILE}";

    printWarning "Composer has been installed";
else
    printWarning "Composer has already installed";
fi

composer about

# update composer to latest version
composer selfupdate