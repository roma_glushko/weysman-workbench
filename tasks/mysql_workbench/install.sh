#!/usr/bin/env bash

if [ ! $(isInstalled mysql-workbench ) ];
then
    sudo apt-get install mysql-workbench-community
    printSuccess "MySql WorkBench has been installed"
else
    printWarning "MySql WorkBench has been already installed"
fi

mysql-workbench --version