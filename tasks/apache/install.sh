#!/usr/bin/env bash

if [ $(isInstalled apache2) ]
then
    printSuccess "Apache2 has been already installed"
else
    sudo apt-get install apache2
fi

apache2 -v

#enable mod-rewrite

sudo a2enmod rewrite