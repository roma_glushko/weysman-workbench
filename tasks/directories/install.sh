#!/usr/bin/env bash

if [ ! -d "${PROJECTS_DIR}" ]
then
    mkdir "${PROJECTS_DIR}";
    sudo chown -R ${CURRENT_USER}:${CURRENT_USER} ${PROJECTS_DIR};

    printSuccess "${PROJECTS_DIR} has been created";
else
    printWarning "${PROJECTS_DIR} has been already created";
fi

if [ ! -d "${APP_DIR}" ]
then
    mkdir "${APP_DIR}";
    sudo chown -R ${CURRENT_USER}:${CURRENT_USER} ${APP_DIR};

    printSuccess "${APP_DIR} has been created";
else
    printWarning "${APP_DIR} has been already created"
fi

if [ ! -d "${LIB_DIR}" ]
then
    mkdir "${LIB_DIR}";
    sudo chown -R ${CURRENT_USER}:${CURRENT_USER} ${LIB_DIR};

    printSuccess "${LIB_DIR} has been created";
else
    printWarning "${LIB_DIR} has been already created";
fi
