#!/usr/bin/env bash

if [ ! $(isInstalled docker) ];
then
    curl -fsSL https://get.docker.com/ | sh
    sudo usermod -aG docker ${CURRENT_USER}
    printSuccess "Docker has been installed"
else
    printWarning "Docker has been already installed"
fi

docker run hello-world