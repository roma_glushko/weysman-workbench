#!/usr/bin/env bash

echo "=====================";
echo "Hello world task";
echo "1. Test variable exporting:";
echo "CWD: ${CWD}";
echo "2. Test function exporting:";
hello
echo "3. Colored output:"
printError "This is a error"
printWarning "This is a warning"
printSuccess "This is a success"
echo "=====================";
