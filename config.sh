#!/usr/bin/env bash

#current directory of the project
export CWD=$(pwd);

# directory that contains all of the task to do
export TASKS_DIR="./tasks/";
export TASK_ENTRY_POINT="install.sh";
export DEPENDENCY_FILE="dependency.list";
export TEMP_DIR="${CWD}/temp";
export LOCAL_BIN="/usr/local/bin"

export PROJECTS_DIR="${HOME}/Projects";
export APP_DIR="${HOME}/Documents/app";
export LIB_DIR="${HOME}/Documents/lib1"

export CURRENT_USER="weyman";

export RED_TEXT_COLOR=`tput setaf 1`;
export GREEN_TEXT_COLOR=`tput setaf 2`;
export YELLOW_TEXT_COLOR=`tput setaf 3`;
export RESET_TEXT_COLOR=`tput sgr0`;
