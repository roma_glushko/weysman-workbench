#!/usr/bin/env bash

CONFIG_FILE="./config.sh";
API_FILE="./api.sh";

if [ ! -x "${CONFIG_FILE}" ]
then
    sudo chmod +x ${CONFIG_FILE};
fi

# include the configuration file
. ${CONFIG_FILE}

if [ ! -x "${API_FILE}" ]
then
    sudo chmod +x ${API_FILE};
fi

# include the api file
. ${API_FILE}


if [ ! -d ${TASK_DIR} ]
then
    echo "The tasks directory doesn't exists!";
    exit;
fi

# run each of the existed tasks

TASKS_DEPENDENCY_LIST="";

cd ${TASKS_DIR};

for TASK in `ls -l | grep ^d`
do
    export CTD="${TASK}/";
    TASK_FILE="${CTD}${TASK_ENTRY_POINT}";
    if [ -f ${TASK_FILE} ]
    then
        TASK_DEPENDENCY_FILE="${CTD}${DEPENDENCY_FILE}";
        if [ -f ${TASK_DEPENDENCY_FILE} ]
        then
            DEPENDENCIES=(`cat ${TASK_DEPENDENCY_FILE}`);

            for DEPENDENCY in "${DEPENDENCIES[@]}"
            do
                TASKS_DEPENDENCY_LIST+=" ${TASK} ${DEPENDENCY}";
            done

        else
            #there is no dependencies in task
            TASKS_DEPENDENCY_LIST+=" ${TASK} ${TASK}";
        fi
    fi
done

SORTED_TASKS=(`sortDependencies "${TASKS_DEPENDENCY_LIST}"`);

for TASK in "${SORTED_TASKS[@]}";
do
    # current task dir
    export CTD="${TASK}/";

    TASK_FILE="${TASK}/${TASK_ENTRY_POINT}";
    if [ -f ${TASK_FILE} ]
    then
        sudo chmod +x ${TASK_FILE}
        ${TASK_FILE}

        printf "\n";
    fi
done