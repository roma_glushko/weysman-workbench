#!/usr/bin/env bash

function hello()
{
    echo 'Hello))';
}

export -f hello;

function printError()
{
    echo "${RED_TEXT_COLOR}$1${RESET_TEXT_COLOR}";
}

export -f printError

function printWarning()
{
    echo "${YELLOW_TEXT_COLOR}$1${RESET_TEXT_COLOR}";
}

export -f printWarning

function printSuccess()
{
    echo "${GREEN_TEXT_COLOR}$1${RESET_TEXT_COLOR}";
}

export -f printSuccess

function isInstalled()
{
    which $1;
}

export -f isInstalled

function sortDependencies()
{
    echo $1 | tsort | tac;
}

export -f sortDependencies